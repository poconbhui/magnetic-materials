import math

# Some parameter calculators
def uniaxial_anisotropy_constant(material):
    # Ku from Dunlop & Ozdemir
    Ku = (
        0.1146*abs(material.get("K1", 0.0))
        + 0.0214*abs(material.get("K2", 0.0))
    )
    return Ku

def magnetic_anisotropy_constant(material):
    # Kd from Rave et al (10.1016/S0304-8853(98)00328-X)
    Js = material["Ms"]
    mu0 = 4*math.pi*1e-7
    Kd = (mu0*Js**2)/(2)
    return Kd

def anisotropy_exchange_length(material):
    A = abs(material["A"])
    Ku = uniaxial_anisotropy_constant(material)
    return math.sqrt(A/Ku)

def domain_wall_width(material):
    return math.pi*anisotropy_exchange_length(material)

def magnetic_exchange_length(material):
    A = abs(material["A"])
    Kd = magnetic_anisotropy_constant(material)
    return math.sqrt(A/Kd)



def generate_elastic_matrix(material):
    # Generate matrix C from c11, c12, c44
    # Elasticity matrix, for use with voigt notation.
    material["C"] = (
        (material["c11"], TM60["c12"], TM60["c12"], 0.0, 0.0, 0.0),
        (material["c12"], TM60["c11"], TM60["c12"], 0.0, 0.0, 0.0),
        (material["c12"], TM60["c12"], TM60["c11"], 0.0, 0.0, 0.0),
        (0.0,         0.0,         0.0,         material["c44"], 0.0, 0.0),
        (0.0,         0.0,         0.0,         0.0, material["c44"], 0.0),
        (0.0,         0.0,         0.0,         0.0, 0.0, material["c44"])
    )

def generate_magnetostrictive_coupling(material):
    # Generate magnetoelastic coupling constants from C, lambdas
    material["B1"] = material["lmbda100"]*(-3./2.)*(
        material["c11"] - material["c12"]
    ) # m^3/N
    material["B2"] = material["lmbda111"]*(-3.)*material["c44"] # m^3/N
    material["B"] = (material["B1"], material["B2"])

def generate_magnetostrictive_corrections(material):
    # Assuming K1 measured was actually K1'
    # (Using Sahu and Moskowitz 1995 10.1029/94GL03296)
    material["K1'"] = material["K1"]      # J/m^3
    material["Klmbda"] = 9./4.*(
        (material["c11"]-material["c12"])*material["lmbda100"]**2
        - 2.*material["c44"]*material["lmbda111"]**2
    )
    material["K1"] = material["K1'"] - material["Klmbda"]
    

#
# Constants for Titanomagnetite (TM60)
# Provided by Karl Fabian
#
TM60 = {"name": "TM60"}

# Magnetic Constants
TM60["K1"] = 2.02e3       # J/m^3
TM60["A"]  = 1.82349e-12  # J/m
TM60["Ms"] = 1.15738e5    # A/m


# Elastic Constants (N/m^2)
TM60["c11"] = 1.36468e11
TM60["c12"] = 5.30848e10
TM60["c44"] = 6.28431e10

# Magnetostriction constants
# (Sahu & Moskowiz 1995)
TM60["lmbda111"] = 95.4e-6  # non-dim
TM60["lmbda100"] = 142.5e-6 # non-dim

generate_elastic_matrix(TM60)
generate_magnetostrictive_coupling(TM60)
generate_magnetostrictive_corrections(TM60)



#
# Constants for Magnetite at 20 C (also Karl)
#

Magnetite = {"name": "magnetite"}

# Magnetic Constants
Magnetite["K1"] = -1.32658e4   # J/m^3
Magnetite["A"]  = 1.33487e-11  # J/m
Magnetite["Ms"] = 4.80768e5    # A/m

# Elastic Constants (N/m^2)
Magnetite["c11"] = 2.73e11
Magnetite["c12"] = 1.06e11
Magnetite["c44"] = 9.7e10

# Magnetostriction constants
Magnetite["lmbda111"] = 8e-5   # non-dim
Magnetite["lmbda100"] = -2e-5  # non-dim

generate_elastic_matrix(Magnetite)
generate_magnetostrictive_coupling(Magnetite)
generate_magnetostrictive_corrections(Magnetite)


#
# Constants for Iron
#
Iron = {"name": "iron"}

# Magnetic Constants
Iron["K1"] = 4.2e5  * 10**-1  # J/m^3
Iron["A"]  = 2.0e-6 * 10**-5  # J/m
Iron["Ms"] = 1707.0 * 10**3   # A/m
